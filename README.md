# Releasr!

Releasr is a tool to help you push releases using Git.

# Install

`npm install -[D|g] releasr.js`

# Usage

`npx releasr.js <major|minor|patch>`

```
Usage: releasr.js [options] <release-type>

Options:
  -V, --version                      output the version number
  -b, --trunk-branch <trunk-branch>  The branch that you must contain the current HEAD commit to make a release (default: "master")
  -G, --no-gitlab                    if specified, do not attempt to merge versioning branch with trunk branch
  -r, --remote <remote>              Name of remote (default: "origin")
  -c, --changelogFile <filename>     Changelog filename (default: "CHANGELOG.md")
  -d, --debug                        Debug mode
  -h, --help                         display help for command
```

If installed globally the `releasr.js` command will become available to the port.  If installed locally then you can run
`npx run releasr.js` from your project root.

Releasr will look for a `.releasrrc` file in your home folder, and current directory.  There is an example `.releasrrc`
included in the repository root. 

Releasr will
- Make sure we have the latest tags available to us
- Check to see if the current commit is available in the trunk branch
- Creates a versioning branch, from the current HEAD
- Generate a changelog (using [generate-changelog](https://www.npmjs.com/package/generate-changelog))
- Bump the version as appropriate (`major`, `minor` or `patch`) (See [npm version](https://docs.npmjs.com/cli/version)) 
- Push the versioning branch to the remote
- _If Gitlab credentials supplied_ Merge the remote versioning branch with the remote trunk branch
- _If a Dockerfile is present and image / container repository is defined_ Build and push the docker image to the remote container
- Clean up (remove versioning branches, pull down newly merged trunk branch)
