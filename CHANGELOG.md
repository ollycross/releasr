#### 1.3.6 (2020-07-06)

##### Bug Fixes

* **gitlab:**  Brief wait between create and accept merge request to let Gitlab catch up ([f6546557](git+ssh://git@gitlab.com/ollycross/releasr/commit/f65465570b53a5d451f12dada2a691dc7a97c1cc))


#### 1.3.5 (2020-06-11)

##### New Features

* **logging:**  Better logging ([947e8dc3](git+ssh://git@gitlab.com/ollycross/releasr/commit/947e8dc30a40a9232e73c919ed7240ee14144555))

##### Bug Fixes

* **merging:**  Incorrect import ([796ba228](git+ssh://git@gitlab.com/ollycross/releasr/commit/796ba2281d51d4ab6870df4816143187309515d2))


#### 1.3.4 (2020-06-11)

##### Bug Fixes

* **ci:**  Only publish on version tags ([fff9ee8e](git+ssh://git@gitlab.com/ollycross/releasr/commit/fff9ee8ed320e59c6bdb3836eb38180bac43ae4d))


#### 1.3.3 (2020-06-11)

##### Bug Fixes

* **ci:**  NPM Auth ([21fb7bd1](git+ssh://git@gitlab.com/ollycross/releasr/commit/21fb7bd1358df9d24c6612c6fd9d37581003864a))


#### 1.3.2 (2020-06-11)

##### Bug Fixes

* **ci:**  NPM Auth ([42e0f071](git+ssh://git@gitlab.com/ollycross/releasr/commit/42e0f071ab1b41c3244daa1993cbc4b65fc94725))


#### 1.3.1 (2020-06-11)

##### Bug Fixes

* **ci:**  NPM Auth ([94825686](git+ssh://git@gitlab.com/ollycross/releasr/commit/94825686360cde9efc456efac8d612d489a4b096))


### 1.3.0 (2020-06-11)

##### Chores

* **tidy:**  Rm unused var ([191f47a8](git+ssh://git@gitlab.com/ollycross/releasr/commit/191f47a8646337e2e5fa505a613876a2432f1f05))

##### New Features

* **ci:**  Publish to NPM on new tag ([06d2545e](git+ssh://git@gitlab.com/ollycross/releasr/commit/06d2545e063f01c49968d6d9027e294ac52c6300))
* **errors:**
  *  Better error handling ([71abb5c2](git+ssh://git@gitlab.com/ollycross/releasr/commit/71abb5c2e4d102f4d53e3573dc22c7ad1668c11a))
  *  Better error handling ([2f4769b9](git+ssh://git@gitlab.com/ollycross/releasr/commit/2f4769b9e0b5506eb60f245a50a36ec3d963c085))
* **local:**  Update local branches after merging remote ones ([b0c2ddd8](git+ssh://git@gitlab.com/ollycross/releasr/commit/b0c2ddd84c4f4824b2d063cc189d3a2639db35ed))


#### 1.2.21 (2020-06-10)

##### Chores

* **refactoring:**  Add bin entries ([6cad623d](git+ssh://git@gitlab.com/ollycross/releasr/commit/6cad623d0493e3ad7c2a5ee116522ed1837a2ad6))

##### Bug Fixes

* **options:**  Fetch parent options in subcommands ([ccaac83f](git+ssh://git@gitlab.com/ollycross/releasr/commit/ccaac83f2e0ff1f317f0f79be964bf6720570a5d))
* **checks:**  Return explicit bool when checking working directoy clean ([a02c1051](git+ssh://git@gitlab.com/ollycross/releasr/commit/a02c105104cfc1a5b8aaa28e9dc09536f9133739))


#### 1.2.20 (2020-06-02)

##### Chores

* **ignore:**  Ignore PHPStorm .idea folder ([790ac224](git+ssh://git@gitlab.com/ollycross/releasr/commit/790ac224373f526e851c5efdf2f2570751812ec8))

##### Documentation Changes

* **README:**  Complete documentation ([4725f9d5](git+ssh://git@gitlab.com/ollycross/releasr/commit/4725f9d58c3670c142f8a69733aa6fdbfb19fce1))


#### 1.2.19 (2020-05-07)

##### Bug Fixes

* **versioning:**  Incorrect non-patch version numbers ([a6617899](git+ssh://git@gitlab.com/ollycross/releasr/commit/a6617899b3cdb086843d4115b0f250b221e5eae3))


#### 1.2.17 (2020-05-06)

##### Chores

* **changelog:**  Simplified changelog checks ([3c23f286](git+ssh://git@gitlab.com/ollycross/releasr/commit/3c23f286cbbbfb792efd53953dfff8ade80cdf76))

##### New Features

* **branches:**  Add option to not attempt remote merge ([1a9ee027](git+ssh://git@gitlab.com/ollycross/releasr/commit/1a9ee02706cb38bc65ffdb77bd9df49ab43825af))

##### Bug Fixes

* **debug:**  Fixed typo in debug ([3f8f5cb4](git+ssh://git@gitlab.com/ollycross/releasr/commit/3f8f5cb4791e2f73f40c18a86718be59ce19056f))


#### 1.2.16 (2020-05-05)

##### Bug Fixes

* **changelog:**  RM Unwanted debug info ([9268702a](git+ssh://git@gitlab.com/ollycross/releasr/commit/9268702a2cdb6fb76675b7f200b1ef160a47730c))


#### 1.2.15 (2020-05-05)

##### Documentation Changes

* **Readme:**  Add help output to README ([89144d61](git+ssh://git@gitlab.com/ollycross/releasr/commit/89144d61b108050be1761d122478cad40d20dd08))

##### Bug Fixes

* **changelog:**  RM Unwanted changelog call ([b7fc0d88](git+ssh://git@gitlab.com/ollycross/releasr/commit/b7fc0d883c207cb0c813656eba403ba17cabe1e2))


#### 1.2.14 (2020-05-05)

##### Documentation Changes

* **Readme:**  Add usage instructions to README ([47ef30eb](git+ssh://git@gitlab.com/ollycross/releasr/commit/47ef30eb2cb4e962a936b2f89a1f6218936d01b9))


#### 1.2.13 (2020-05-05)

##### Documentation Changes

* **Readme:**  Add usage instructions to README ([47ef30eb](git+ssh://git@gitlab.com/ollycross/releasr/commit/47ef30eb2cb4e962a936b2f89a1f6218936d01b9))

#### 1.2.13 (2020-05-05)

##### Bug Fixes

* **changelog:**  Prepend to changelog, don't overwrite! ([90a7b35d](git+ssh://git@gitlab.com/ollycross/releasr/commit/90a7b35d0400d645e34b2d58eb3ae2b4de79f1c6))

##### Other Changes

* **housekeeping:**  Prettify ([2b77d430](git+ssh://git@gitlab.com/ollycross/releasr/commit/2b77d4300851a4cd353a8348b048797bc4e0ac51))


#### 1.2.12 (2020-05-05)

##### Bug Fixes

* **changelog:**  Prepend to changelog, don't overwrite! ([90a7b35d](git+ssh://git@gitlab.com/ollycross/releasr/commit/90a7b35d0400d645e34b2d58eb3ae2b4de79f1c6))

##### Other Changes

* **housekeeping:**  Prettify ([2b77d430](git+ssh://git@gitlab.com/ollycross/releasr/commit/2b77d4300851a4cd353a8348b048797bc4e0ac51))

#### 1.2.12 (2020-05-05)

##### Documentation Changes

* **Readme:**  Add installation instructions to README ([a9b48297](git+ssh://git@gitlab.com/ollycross/releasr/commit/a9b48297efab56ecc8cae2cec02f04e5a0e5d89a))

