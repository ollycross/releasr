require('./utils/process-config.js');

const { argv } = process;
const commander = require('commander');
const chillWinston = require('chill-winston');

const doAction = require('./utils/do-action');
const error = require('./utils/error');
const fetchRemote = require('./utils/fetch-remote');
const ensureWorkingDirectoryClean = require('./utils/ensure-working-directory-clean');
const isLocalUpToDate = require('./utils/is-local-up-to-date');
const checkTrunkBranch = require('./utils/check-trunk-branch');
const generateAndCommitChangelog = require('./utils/generate-and-commit-changelog');
const bumpVersion = require('./utils/bump-version');
const pushToRemote = require('./utils/push-to-remote');
const getLatestTag = require('./utils/get-latest-tag');
const getCurrentBranch = require('./utils/get-current-branch');
const createVersioningBranch = require('./utils/create-versioning-branch');
const syncLatestVersion = require('./utils/sync-latest-version');
const cleanUpBranches = require('./utils/clean-up-branches');
const mergeRemoteTrunkWithCurrentBranch = require('./utils/merge-remote-trunk-with-current-branch');

const { version: releasrVersion } = require('../package.json');

commander
  .version(releasrVersion)
  .option(
    '-b, --trunk-branch <trunk-branch>',
    'The branch that you must contain the current HEAD commit to make a release',
    'master',
  )
  .option('-d, --debug', 'Debug mode')
  .option('-r, --remote <remote>', 'Name of remote', 'origin');

commander.command('trunk').action(
  doAction(async ({ parent: { trunkBranch, debug, remote } }) => {
    const logger = chillWinston(debug ? 'debug' : 'info');

    await ensureWorkingDirectoryClean(logger);

    const currentBranch = await getCurrentBranch(logger);

    await pushToRemote(remote, currentBranch, trunkBranch, false, true, logger);

    await mergeRemoteTrunkWithCurrentBranch(remote, trunkBranch, logger);
  }),
);

commander
  .arguments('<release-type>')

  .option(
    '-G, --no-gitlab',
    'if specified, do not attempt to merge versioning branch with trunk branch',
    true,
  )

  .option(
    '-c, --changelogFile <filename>',
    'Changelog filename',
    'CHANGELOG.md',
  )
  .action(
    doAction(
      async (
        releaseType,
        { trunkBranch, remote, changelogFile, debug, gitlab },
      ) => {
        const logger = chillWinston(debug ? 'debug' : 'info');

        let currentBranch;
        let versionBranch;

        try {
          logger.info('Checking we are in a state to release...');

          await ensureWorkingDirectoryClean(logger);

          currentBranch = await getCurrentBranch(logger);

          await fetchRemote(remote, logger);
          if (!(await isLocalUpToDate(logger))) {
            throw error(
              'Local branch is not up to date with remote',
              'localNotUpToDate',
            );
          }

          const currentTag = (await getLatestTag(logger)) || 'v0.0.1';

          if (debug) {
            logger.debug(
              currentTag
                ? `Current version is ${currentTag}.`
                : 'No current version available',
            );
          }

          await checkTrunkBranch(remote, trunkBranch, logger);

          logger.info('Preparing release...');

          const latestVersion = await syncLatestVersion(logger);

          versionBranch = await createVersioningBranch(
            latestVersion,
            releaseType,
            logger,
          );

          await generateAndCommitChangelog(releaseType, changelogFile, logger);
          await bumpVersion(releaseType, logger);

          logger.info('Releasing...');
          const version = await getLatestTag(logger);

          await pushToRemote(
            remote,
            versionBranch,
            trunkBranch,
            true,
            gitlab,
            logger,
          );

          logger.info(`Released ${version || 'none'} to remote '${remote}'.`);
          return 0;
        } finally {
          if (versionBranch) {
            await cleanUpBranches(
              currentBranch,
              trunkBranch,
              versionBranch,
              logger,
            );
          }
        }
      },
    ),
  );

commander.parse(argv);
