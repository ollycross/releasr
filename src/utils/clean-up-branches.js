const childProcessPromise = require('./child-process-promise');

const cleanUpBranches = async (
  originalBranch,
  trunkBranch,
  releaseBranch,
  logger,
) => {
  const { spawn } = childProcessPromise(logger);
  logger.info('Cleaning up...');

  logger.debug(`Checking out original branch '${originalBranch}'`);

  await spawn('git', ['checkout', trunkBranch]);

  await spawn('git', ['pull']);

  await spawn('git', ['checkout', originalBranch]);

  logger.debug(`Deleting release branch '${releaseBranch}'.`);

  await spawn('git', ['branch', '-D', releaseBranch]);
};

module.exports = cleanUpBranches;
