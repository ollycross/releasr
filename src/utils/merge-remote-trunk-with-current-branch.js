const childProcessPromise = require('./child-process-promise');
const fetchRemote = require('./fetch-remote');
const getCurrentBranch = require('./get-current-branch');

const mergeRemoteTrunkWithCurrentBranch = async (
  remote,
  trunkBranch,
  logger,
) => {
  const { spawn } = childProcessPromise(logger);

  await fetchRemote(remote, logger);

  const currentBranch = await getCurrentBranch(logger);
  const remoteTrunk = `${remote}/${trunkBranch}`;

  logger.info(
    `Merging remote trunk branch '${remoteTrunk}' with local branch '${currentBranch}'`,
  );

  return spawn('git', ['merge', remoteTrunk]);
};

module.exports = mergeRemoteTrunkWithCurrentBranch;
