const error = require('./error');

const getNextVersionNumber = (currentVersion, releaseType, logger) => {
  logger.debug(`Current version is: '${currentVersion}'.`);
  const stripped = currentVersion.replace(/[^\d.]/, '');

  const match = stripped.match(/(\d+).(\d+).(\d+)/);

  if (!match) {
    throw error(
      `Can't find next version; Invalid Version Number '${currentVersion}'.`,
      'invalidVersionNumber',
    );
  }

  let [, major, minor, patch] = match;

  switch (releaseType) {
    case 'major':
      major = +major + 1;
      minor = 0;
      patch = 0;
      break;
    case 'minor':
      minor = +minor + 1;
      patch = 0;
      break;
    case 'patch':
      patch = +patch + 1;
      break;
    default:
      break;
  }

  return `v${major}.${minor}.${patch}`;
};

module.exports = getNextVersionNumber;
