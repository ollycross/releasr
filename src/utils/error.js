const error = (message = 'Unknown error', name = 'unspecified') => {
  const ERROR_CODES = {
    unspecified: 1,
    unsupportedReleaseType: 2,
    commitNotOnTrunk: 3,
    localNotUpToDate: 4,
    invalidVersionNumber: 5,
    workingDirNotClean: 6,
  };

  const e = new Error(message);
  e.name = name;
  e.code = ERROR_CODES[name] || 1;

  return e;
};

module.exports = error;
