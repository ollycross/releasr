const { generate } = require('generate-changelog');
const { existsSync, readFileSync, writeFileSync } = require('fs');

const childProcessPromise = require('./child-process-promise');
const error = require('./error');

const generateAndCommitChangelog = async (
  releaseType,
  changelogFile,
  logger,
) => {
  const { spawn } = childProcessPromise(logger);
  const checkChangelogFlag = () => {
    const supportedReleaseTypes = ['major', 'minor', 'patch'];

    if (!supportedReleaseTypes.includes(releaseType)) {
      throw error(
        `Unsupported release type: '${releaseType}'`,
        'unsupportedReleaseType',
      );
    }

    return supportedReleaseTypes[releaseType];
  };

  if (changelogFile) {
    checkChangelogFlag();

    logger.info('Generating changelog...');

    const prependChangelog = await generate({
      [releaseType]: true,
    });

    const changelogPath = `./${changelogFile}`;

    const currentChangelog = existsSync(changelogPath)
      ? readFileSync(changelogPath).toString()
      : '';

    writeFileSync(
      changelogPath,
      [prependChangelog, currentChangelog].join('\n'),
    );

    logger.debug('Adding changelog to Git...');
    await spawn('git', ['add', 'CHANGELOG.md']);
    await spawn('git', ['commit', '-m', 'updated CHANGELOG.md']);
  }
};

module.exports = generateAndCommitChangelog;
