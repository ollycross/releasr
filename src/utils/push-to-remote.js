const mergeRemoteBranches = require('./merge-remote-branches');
const childProcessPromise = require('./child-process-promise');

const pushToRemote = async (
  remote,
  toPush,
  trunkBranch,
  tags = false,
  gitlab,
  logger,
) => {
  const { spawn } = childProcessPromise(logger);

  logger.info(`Pushing local '${toPush}' to remote '${remote}/${toPush}'...`);

  await spawn('git', ['push', '--set-upstream', remote, toPush]);

  if (tags) {
    await spawn('git', ['push', remote, '--tags']);
  }

  if (toPush !== trunkBranch) {
    await mergeRemoteBranches(gitlab, remote, toPush, trunkBranch, logger);
  }
};

module.exports = pushToRemote;
