const { exec, spawn, fork, execFile } = require('child_process');
const { promisify } = require('util');

const promisifyEvents = (callable, logger) => (
  command,
  args = [],
  options = {},
) =>
  new Promise((resolve, reject) => {
    let output = [];

    const stderr = [];
    const {
      onStderr = (data) => {
        const message = data.toString().trim();
        logger.debug(message);
        stderr.push(message);
      },
      onStdout = (data) => {
        output = [
          ...output,
          ...data
            .toString()
            .trim()
            .split('\n')
            .map((val) => val.trim()),
        ];
      },
      onClose = (code) => {
        if (code !== 0) {
          const error = new Error(stderr.join('\n'));
          error.code = code;
          reject(error);
        } else {
          resolve(output);
        }
      },
    } = options;

    logger.debug(`Command: \`${command} ${args.join(' ')}\``, options);
    const called = callable(command, args, options);

    if (onStdout) {
      called.stdout.on('data', onStdout);
    }

    if (onStderr) {
      called.stderr.on('data', onStderr);
    }

    if (onClose) {
      called.on('close', onClose);
    }
  });

module.exports = (logger) => ({
  exec: promisify(exec, logger),
  execFile: promisify(execFile, logger),
  spawn: promisifyEvents(spawn, logger),
  fork: promisifyEvents(fork, logger),
});
