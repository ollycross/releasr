const childProcessPromise = require('./child-process-promise');
const getNextVersionNumber = require('./get-next-version-number');

const createVersioningBranch = async (currentVersion, releaseType, logger) => {
  const { spawn } = childProcessPromise(logger);

  const version = getNextVersionNumber(currentVersion, releaseType, logger);

  const branch = `version/${version}`;

  logger.info(`Checking out versioning branch '${branch}'.`);

  await spawn('git', ['checkout', '-b', branch]);

  return branch;
};

module.exports = createVersioningBranch;
