const dotenv = require('dotenv');
const { resolve } = require('path');
const { existsSync } = require('fs');

const locations = [process.cwd(), process.env.HOME];

locations.forEach((location) => {
  const path = resolve(location, '.releasrrc');
  if (existsSync(path)) {
    dotenv.config({
      path,
    });
  }
});
