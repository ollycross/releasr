const childProcessPromise = require('./child-process-promise');

const isLocalUpToDate = async (logger) => {
  const { spawn } = childProcessPromise(logger);
  logger.debug('Checking local is up to date...');
  const allBranches = await spawn('git', ['branch', '-v']);

  const currentBranch = allBranches.find((line) => line.match(/^\*/m));

  return currentBranch.match(/ \[behind \d+\] /) === null;
};

module.exports = isLocalUpToDate;
