const chillWinston = require('chill-winston');

const doAction = (action) => async (...args) => {
  try {
    await action(...args);
  } catch ({ message = 'Unspecified error', code = -1 }) {
    chillWinston().error(`Error (${code}): ${message}`);
    process.exit(code);
  }
};

module.exports = doAction;
