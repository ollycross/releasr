const childProcessPromise = require('./child-process-promise');
const error = require('./error');

const checkTrunkBranch = async (remote, trunkBranch, logger) => {
  const { spawn } = childProcessPromise(logger);

  const getCurrentCommit = async () => spawn('git', ['rev-parse', 'HEAD']);

  const isCommitOnRequiredBranch = async (commit, fullTrunkBranch) => {
    const branches = await spawn('git', ['branch', '-r', '--contains', commit]);

    if (branches.length) {
      logger.debug(`Commit '${commit}' exists on:`, branches);
    } else {
      logger.debug(`Commit '${commit}' does not exist on any remote branches.`);
    }

    const exists = branches.includes(fullTrunkBranch);

    logger.debug(
      `Commit '${commit}' ${
        exists ? 'exists' : 'does not exist'
      } on '${fullTrunkBranch}'.`,
    );

    return exists;
  };

  const [currentCommit] = await getCurrentCommit();
  logger.debug(`Current commit hash: '${currentCommit}'.`);

  const fullTrunkBranch = `${remote}/${trunkBranch}`;
  logger.debug(`Full trunk branch name: '${fullTrunkBranch}'.`);

  logger.debug(
    `Checking current commit exists on trunk branch '${fullTrunkBranch}'...`,
  );
  if (trunkBranch === 'false') {
    logger.warn(
      'Trunk branch is false, not checking whether your commit is in the main trunk.',
    );
  } else if (
    !(await isCommitOnRequiredBranch(currentCommit, fullTrunkBranch))
  ) {
    throw error(
      `The current commit does not exist on trunk branch '${fullTrunkBranch}'.`,
      'commitNotOnTrunk',
    );
  }
};

module.exports = checkTrunkBranch;
