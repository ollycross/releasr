const childProcessPromise = require('./child-process-promise');

const getLatestTag = async (logger) => {
  const { spawn } = childProcessPromise(logger);
  try {
    const [out] = await spawn('git', ['describe', '--abbrev=0']);

    return out;
  } catch (e) {
    if (e.code === 128) {
      return null;
    }

    throw e;
  }
};

module.exports = getLatestTag;
