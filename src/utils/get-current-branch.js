const childProcessPromise = require('./child-process-promise');

const getLatestTag = async (logger) => {
  const { spawn } = childProcessPromise(logger);

  const [out] = await spawn('git', ['rev-parse', '--abbrev-ref', 'HEAD']);

  return out;
};

module.exports = getLatestTag;
