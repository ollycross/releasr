const childProcessPromise = require('./child-process-promise');

const fetchRemote = async (remote, logger) => {
  const { spawn } = childProcessPromise(logger);
  logger.debug('Fetching remote...');

  return spawn('git', ['fetch', '--quiet', remote]);
};

module.exports = fetchRemote;
