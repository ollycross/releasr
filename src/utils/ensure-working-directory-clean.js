const error = require('./error');
const isWorkingDirectoryClean = require('./is-working-directory-clean');

const ensureWorkingDirectoryClean = async (logger) => {
  if (!(await isWorkingDirectoryClean(logger))) {
    throw error(
      'Working directory is not clean; please commit or stash any changes and try again.',
      'workingDirNotClean',
    );
  }
};

module.exports = ensureWorkingDirectoryClean;
