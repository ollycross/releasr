const semverSort = require('semver-sort');
const childProcessPromise = require('./child-process-promise');

const syncLatestVersion = async (logger) => {
  const { spawn } = childProcessPromise(logger);

  logger.debug('Fetching latest version number from Git...');
  const allVersions = await spawn('git', ['tag']);
  semverSort.desc(allVersions);

  const [latest] = allVersions;

  await spawn('npm', [
    'version',
    '--allow-same-version',
    '--no-git-tag-version',
    latest,
  ]);

  if ((await spawn('git', ['status', '--porcelain'])).length) {
    await spawn('git', ['add', 'package.json', 'package-lock.json']);
    await spawn('git', ['commit', '-m', 'Versioning']);
  }

  return latest;
};

module.exports = syncLatestVersion;
