const childProcessPromise = require('./child-process-promise');

const buildAndPushImage = async (imagePath, version, logger) => {
  const { spawn } = childProcessPromise(logger);
  const tagLatest = `${imagePath}:latest`;
  const tagVersion = `${imagePath}:${version}`;

  logger.debug('Building Docker image...');
  await spawn('docker', ['build', '-t', tagLatest, '.']);

  await spawn('docker', ['tag', tagLatest, tagVersion]);

  logger.debug('Pushing Docker image...');
  await spawn('docker', ['push', tagLatest]);
  await spawn('docker', ['push', tagVersion]);
};

module.exports = buildAndPushImage;
