const childProcessPromise = require('./child-process-promise');
const error = require('./error');

const getRemoteNamespacedProject = async (remote, logger) => {
  const { spawn } = childProcessPromise(logger);

  const [remoteUrl] = await spawn('git', [
    'remote',
    'get-url',
    '--push',
    remote,
  ]);

  const [, namespacedProject] = remoteUrl.match(/(\w+\/[\w-]+)\.git$/);

  if (!namespacedProject) {
    throw error(`Could not parse namespaced project from ${remoteUrl}.`);
  }

  return namespacedProject;
};

module.exports = getRemoteNamespacedProject;
