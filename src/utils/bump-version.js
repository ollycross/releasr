const childProcessPromise = require('./child-process-promise');

const bumpVersion = async (releaseType, logger) => {
  const { spawn } = childProcessPromise(logger);
  logger.info('Bumping version...');

  return spawn('npm', ['version', releaseType]);
};

module.exports = bumpVersion;
