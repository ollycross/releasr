const axios = require('axios');

const getRemoteNamespacedProject = require('./get-remote-namespaced-project');
const fetchRemote = require('./fetch-remote');

const mergeRemoteBranches = async (
  gitlab,
  remote,
  sourceBranch,
  targetBranch,
  logger,
) => {
  const {
    GITLAB_PERSONAL_TOKEN,
    GITLAB_SERVER = 'https://gitlab.com',
  } = process.env;

  if (!gitlab) {
    logger.warn(
      `--no-gitlab option specified, so not attempting to merge remote versioning branch.  You must manually merge the versioning branch \`${sourceBranch}\` into the trunk branch \`${targetBranch}\`.`,
    );
    return false;
  }

  if (!GITLAB_PERSONAL_TOKEN) {
    logger.warn(
      `Gitlab credentials not present.  You must manually merge the versioning branch \`${sourceBranch}\` into the trunk branch \`${targetBranch}\`. Create a .releasrrc file in your project root to fix this error. (See https://gitlab.com/ollycross/releasr)`,
    );
    return false;
  }

  logger.info(
    `Merging version branch \`${sourceBranch}\` with trunk branch \`${targetBranch}\` on remote \`${remote}\``,
  );

  const remoteNamespacedProject = await getRemoteNamespacedProject(
    remote,
    logger,
  );

  const instance = axios.create({
    baseURL: `${GITLAB_SERVER}/api/v4/projects/${encodeURIComponent(
      remoteNamespacedProject,
    )}/merge_requests`,
    headers: { 'Private-Token': GITLAB_PERSONAL_TOKEN },
  });

  const createOpts = {
    source_branch: sourceBranch,
    target_branch: targetBranch,
    title: sourceBranch,
    remove_source_branch: true,
  };

  try {
    logger.info('Creating merge request...');
    logger.debug(createOpts);

    const {
      data: { iid: mergeRequestId },
    } = await instance.post('', createOpts);

    await new Promise((r) => setTimeout(r, 1000));

    const acceptUrl = `/${mergeRequestId}/merge`;

    const acceptOpts = {
      should_remove_source_branch: true,
    };

    logger.info('Accepting merge request...');
    logger.debug({ acceptUrl });
    logger.debug({ acceptOpts });

    await instance.put(acceptUrl, acceptOpts);
  } catch (e) {
    logger.warn(
      `Could not merge automatically.  You must manually merge the versioning branch \`${sourceBranch}\` into the trunk branch \`${targetBranch}\`.`,
    );
    logger.debug(e.message);
    logger.debug(e.response.data);

    return false;
  }

  await fetchRemote(remote, logger);

  return true;
};

module.exports = mergeRemoteBranches;
