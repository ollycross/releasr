const childProcessPromise = require('./child-process-promise');

const isWorkingDirectoryClean = async (logger) => {
  const { spawn } = childProcessPromise(logger);

  const dirt = await spawn('git', ['status', '--porcelain']);

  return dirt.length === 0;
};

module.exports = isWorkingDirectoryClean;
